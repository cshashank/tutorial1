class Datagen

def initialize(filename)
	@filename=filename
end

def get_data
array_country=Array.new
array_price=Array.new
require 'csv'
 CSV.foreach(@filename) do|row| 		
 		 array_country<<row[0]
 		 array_price<<row[1].to_f
		 
 end


return array_price,array_country
end
end

class ItemData
	def initialize (country,price)
		@country=country
		@price=price	
	end

	def give_country
		@country
	end

	def give_price
		@price
	end
	def tax_usa
		(TaxUsa.new@price).tax_cal_usa
	end
	def tax_uk
		(TaxUk.new@price).tax_cal_uk
	end

	def tax_india
		(TaxIndia.new@price).tax_cal_india
	end
end


class TaxUsa 

	def initialize price
		@price=price
	end

	def tax_cal_usa
		return Math.sqrt(@price)
	end
end


class TaxUk
	
	def initialize price
		@price=price
	end

	def tax_cal_uk
		return 0.2*(@price)
	end
end


class TaxIndia
	def initialize price
		@price=price
	end

	def tax_cal_india
		if @price<=100
		tax=0
		return tax
	elsif @price>100 and @price<=500
		tax=0.05*@price
		return tax
	elsif @price>500
		extra_than500 = @price-500
		tax_extra500 = 0.2*extra_than500
		tax_inbtw = 0.05*(extra_than500-100)
		tax=tax_inbtw+tax_extra500
		return tax
	end
end
end

class OutputWrite
def initialize (length_data,country_array,price_array,tax_array)
@length_data=length_data
@country_array=country_array
@price_array=price_array
@tax_array=tax_array

end

def write

CSV.open('output_oop.csv',"w+b") do |csv|
	csv<<["Country","Price","Tax"]


for num in 1...@length_data	
	
		csv<<[@country_array[num],@price_array[num],@tax_array[num]]
	end
end     
end







data_instance=Datagen.new 'prices.csv'
 price_array,country_array=data_instance.get_data
 tax_array=Array.new
 length_data=price_array.length



for num in 1...price_array.length
	new_item=ItemData.new country_array[num],price_array[num]
	country=new_item.give_country
	price=new_item.give_price

	if country=="india"
		tax_array[num]=new_item.tax_india
	
elsif country=="uk"
	tax_array[num]=new_item.tax_uk
elsif country=="usa"
	tax_array[num]=new_item.tax_usa
else
	tax_array[num]="NA"
end
		
	


end


(OutputWrite.new length_data,country_array,price_array,tax_array).write


end


